import React from 'react'
import './Footer.scss'

const Footer = () => {
    return (
        <div className='footer'>
            <div className="top">
                <div className="item">
                    <h1>Categories</h1>
                    <span>Women</span>
                    <span>Men</span>
                    <span>Shoes</span>
                    <span>Accessorries</span>
                    <span>New Arrivals</span>
                </div>
                <div className="item">
                    <h1>Links</h1>
                    <span>FAQ</span>
                    <span>Pages</span>
                    <span>Stores</span>
                    <span>Cookies</span>
                </div>
                <div className="item">
                    <h1>About</h1>
                    <span>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                        Officia, quaerat hic.
                        Perspiciatis reprehenderit quaerat ipsa tempore corrupti minus earum placeat,
                        quis quas amet illum.
                    </span>
                </div>
                <div className="item">
                    <h1>Contact</h1>
                    <span>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Est, ex. Minima magni voluptatibus,
                        labore blanditiis nam culpa dolores ut impedit consequatur, a reprehenderit,
                        omnis sequi eaque et laudantium provident sint.
                    </span>
                </div>

            </div>
            <div className="bottom">
                <div className="left">
                    <span className="logo">SarchyWorld</span>
                    <span className="copyright">&copy; Copyright 2023. All Rights  Reserved</span>
                </div>
                <div className="right">
                    <img src="/images/payment.png" alt="" srcset="" />
                </div>
            </div>
        </div>
    )
}

export default Footer