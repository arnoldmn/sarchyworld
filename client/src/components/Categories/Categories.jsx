import React from 'react'
import './Categories.scss'
import { Link } from 'react-router-dom'

const Categories = () => {
    return (
        <div className='categories'>
            <div className="col">
                <div className="row">
                    <img src="https://images.pexels.com/photos/2421374/pexels-photo-2421374.jpeg?auto=compress&cs=tinysrgb&w=600" alt="" />

                    <Link>
                        <button className="link" to="/products/1">
                            Sale
                        </button>
                    </Link>
                </div>
                <div className="row">
                    <img src="https://images.pexels.com/photos/1020488/pexels-photo-1020488.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="" />
                    <Link>
                        <button className="link" to="/products/1">
                            Shoes
                        </button>
                    </Link>
                </div>
            </div>
            <div className="col">
                <div className="row">
                    <img src="https://images.pexels.com/photos/837140/pexels-photo-837140.jpeg?auto=compress&cs=tinysrgb&w=600" alt="" />
                    <Link>
                        <button className="link" to="/products/1">
                            Semi casual
                        </button>
                    </Link>
                </div>
            </div>
            <div className="col col-l">
                <div className="row">
                    <div className="col">
                        <div className="row">
                            <img src="https://images.pexels.com/photos/794062/pexels-photo-794062.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="" />
                            <Link>
                                <button className="link" to="/products/1">
                                    Sale
                                </button>
                            </Link>
                        </div>
                    </div>
                    <div className="col">
                        <div className="row">
                            <img src="https://images.pexels.com/photos/965324/pexels-photo-965324.jpeg?auto=compress&cs=tinysrgb&w=600" alt="" />
                            <Link>
                                <button className="link" to="/products/1">
                                    Athlesure
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <img src="https://images.pexels.com/photos/157675/fashion-men-s-individuality-black-and-white-157675.jpeg?auto=compress&cs=tinysrgb&w=600" alt="" />
                    <Link>
                        <button className="link" to="/products/1">
                            Casual
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default Categories