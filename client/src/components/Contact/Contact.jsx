import  InstagramIcon from '@mui/icons-material/Instagram'
import FacebookIcon from '@mui/icons-material/Facebook'
import TwitterIcon from '@mui/icons-material/Twitter'
import './Contact.scss'
import React from 'react'

const Contact = () => {
  return (
    <div className='contact'>
      <div className="wrapper">
        <span>GET IN TOUCH WITH US: </span>
        <div className="mail">
          <input type="text" placeholder='Enter your email address...' />
          <button>JOIN US</button>
        </div>
        <div className="icons">
          <FacebookIcon />
          <InstagramIcon />
          <TwitterIcon />
        </div>
      </div>
    </div>
  )
}

export default Contact 