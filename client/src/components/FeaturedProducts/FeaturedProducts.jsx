import React from 'react'
import Card from '../Card/Card'
import './FeaturedProducts.scss'

const FeaturedProducts = ({type}) => {

    const data = [
        {
            id: 1,
            img: "https://image.tfgmedia.co.za/image/1/process/259x259?source=https://cdn.tfgmedia.co.za/02/ProductImages/59901811.jpg",
            img2: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQr9VnmTyUgX54wExqjf2xqSJFpcoa9t9uYmg&usqp=CAU",
            title: "Long Sleeve Graphic T shirt",
            isNew: true,
            oldPrice: 19,
            price: 15,
        },

        {
            id: 2,
            img: "https://images.pexels.com/photos/298863/pexels-photo-298863.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
            title: "Coat",
            isNew: true,
            oldPrice: 19,
            price: 15,
        },

        {
            id: 3,
            img: "https://plus.unsplash.com/premium_photo-1664267832158-bbc72d11e36b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8Y2xvdGhpbmd8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60",
            title: "Shirt",
            isNew: true,
            oldPrice: 19,
            price: 15,
        },
        {
            id: 4,
            img: "https://images.pexels.com/photos/934070/pexels-photo-934070.jpeg?auto=compress&cs=tinysrgb&w=600",
            title: "Skirt",
            isNew: true,
            oldPrice: 19,
            price: 15,
        },

    ]

    return (
        <div className='featuredProducts'>
            <div className="top">
                <h1>{type} products</h1>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                     Illum quibusdam quae neque? 
                     Temporibus quaerat nemo sequi doloremque quibusdam rerum, non molestiae explicabo culpa, totam aut numquam, 
                     itaque vero laboriosam aliquid.
                </p>

            </div>
            <div className="bottom">
                {data.map(item => (
                    <Card item={item} key={item.id}/>
                ))}
            </div>
        </div>
    )
}

export default FeaturedProducts